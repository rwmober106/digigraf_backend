const express = require('express')
const router = express.Router()
const autographController =   require('../controllers/autographController');

// Create a new autograph
router.post('/', autographController.uploadAssetToIPFS);

module.exports = router