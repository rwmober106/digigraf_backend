const express = require('express')
const router = express.Router()
const celebrityController =   require('../controllers/celebrityController');
// Retrieve all celebritys
router.get('/', celebrityController.findAll);
// Create a new celebrity
router.post('/register', celebrityController.create);
// Retrieve a single celebrity with id
router.get('/:id', celebrityController.findById);
// Update a celebrity with id
router.put('/editaccount/:id', celebrityController.update);
// Delete a celebrity with id
router.delete('/:id', celebrityController.delete);
// Sign in
router.post('/login', celebrityController.login);

module.exports = router