const express = require('express')
const router = express.Router()
const requestController =   require('../controllers/requestController');

// Create a new request
router.post('/', requestController.create);
// Retrieve a single request with celebrity_id
router.get('/', requestController.findByCelebrityId);
// Handle request
router.post('/accept', requestController.handleRequest);
// Get request details by customer_id
router.get('/customer', requestController.getRequestById);

module.exports = router