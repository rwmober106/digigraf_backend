'use strict';
const Request = require('../models/request_model');
const fs = require('fs');
const path = require('path')

exports.create = function(req, res) {
    
    //handles null error
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Please provide all required field' });
    }else{
        let customObj = {
            message: req.body.message,
            image_url: '',
            celebrity_id: req.body.celebrity_id,
            customer_id: req.body.customer_id,
            status: 0
        }
        if (req.body.base64image) {
            console.log(req.body.base64image)
            var bitmap = Buffer.from(req.body.base64image, 'base64');
            var url = Date.now() + '.png';
            var rootPath = path.dirname(require.main.filename);
            var filePath = path.join(rootPath, 'images', 'nft', url)
            console.log(filePath);
            fs.writeFileSync(filePath, bitmap, function(err){
                console.log("++++++", err);
            });
            customObj.image_url = url;
        }
        console.log(customObj);
        Request.create(customObj, function(err, request) {
            if (err)
                res.send(err);
            res.json({error: false, message: "Sent Request Successfully", data: request});
        });
    }
};
exports.findByCelebrityId = function(req, res) {
    Request.findByCelebrityId(req.query.celebrity_id, function(err, request) {
        if (err)
            res.send(err);
        res.status(200).send({error: false, message: "Success", data: request});
    });
};

exports.handleRequest = function(req, res) {   
    Request.handleRequest(req.body.status, req.body.request_id, function(err, request) {
        if (err)
            res.send(err);
        res.status(200).send({error: false, message: "Success", data: request});
    });
};

exports.getRequestById = function(req, res) {
    Request.getRequestById(req.query.customer_id, function(err, result) {
        if (err)
            res.send(err);
        res.status(200).send({error: false, message: "Success", data: result});
    });
};