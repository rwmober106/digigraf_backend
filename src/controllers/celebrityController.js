'use strict';
const Celebrity = require('../models/celebrity_model');
const bcrypt = require('bcrypt');
const fs = require('fs');
const path = require('path')
const FileAPI = require('file-api')
var nodemailer = require('nodemailer')


const { File } = FileAPI
exports.findAll = function(req, res) {
    Celebrity.findAll(function(err, celebrity) {
        console.log('controller')
        if (err)
            res.send(err);

        console.log('res', celebrity);
        res.send({error:false,message:"Success",data:celebrity});
    });
};
exports.create = function(req, res) {
    //handles null error
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Please provide all required field' });
    }else{
        let customObj = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            phone_number: req.body.phone_num,
            celebrity_type: req.body.describe,
            cwallet_address: req.body.cwallet_address,
            password: '',
            cavatar_url: ''
        }

        // // convert base64 string to image
        if (req.body.base64image) {
            const i = req.body.base64image.indexOf('base64,')
            const buffer = Buffer.from(req.body.base64image.slice(i + 7), 'base64')
            const name = `${Math.random().toString(36).slice(-5)}.png`
            const file = new File({buffer, name, type: 'image/png'})
            var url = Date.now() + '.png';
            var rootPath = path.dirname(require.main.filename);
            var filePath = path.join(rootPath, 'images', 'avatar', url)
            fs.writeFileSync(filePath, file.buffer, function(err){
                console.log("++++++", err);
            });
            customObj.cavatar_url = filePath;
        }

        // // hash password
        const saltRounds = 10;        
        bcrypt.genSalt(saltRounds)
            .then(salt => {
                bcrypt.hash(req.body.password, salt)
                .then(hash => {
                    console.log(hash);
                    customObj.password = hash;

                    const new_celebrity = new Celebrity(customObj);
                    Celebrity.create(new_celebrity, function(err, celebrity) {
                        if (err)
                            res.send(err);
                        res.json({error:false,message:"Celebrity added successfully!",data:celebrity});

                        var transporter = nodemailer.createTransport({ 
                            service: 'gmail',
                            // host: 'smtp.boostekstudio.org',
                            // port: '587',
                            auth: { user: 'boostekstudio@gmail.com', pass: 'wsuvbznmpijthjnv' },
                            // secureConnection: false,
                            // tls: { ciphers: 'SSLv3' }
                        });

                        const html = `Hi Brett! <br> ${customObj.first_name} ${customObj.last_name}, ${customObj.celebrity_type} has been registered. The information is ${customObj.email}, ${customObj.phone_number}.`
                        var mailOptions = {
                            from: 'boostekstudio@gmail.com',
                            to: 'brettdebari@gmail.com',
                            subject: 'Digigraf',
                            html: html                
                        };        
                    
                        transporter.sendMail(mailOptions, function(error, info){
                            if (error) {
                                console.log(error);
                            } else {
                                console.log('Email sent: ' + info.response);
                            }
                        })
                    })
                });
            });        
    }
};
exports.findById = function(req, res) {
    Celebrity.findById(req.params.id, function(err, celebrity) {
        if (err)
            res.send(err);
        res.json(celebrity);
    });
};
exports.update = function(req, res) {
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Please provide all required field' });
    }else{
        Celebrity.update(req.params.id, new Celebrity(req.body), function(err, celebrity) {
            if (err)
                res.send(err);
            res.json({ error:false, message: 'Celebrity successfully updated' });
    });
    }
};
exports.delete = function(req, res) {
    Celebrity.delete( req.params.id, function(err, celebrity) {
        if (err)
            res.send(err);
        res.json({ error:false, message: 'Celebrity successfully deleted' });
    });
};

exports.login = function(req, res) {
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Please provide all required field' });
    }else{
        let customObj = {
            email: req.body.email,
            password: req.body.password
        }

        Celebrity.login(customObj, function(err, celebrity) {
            if (err)
                res.send(err);
            res.json({error:false,message:"Login Success",data:celebrity});
        });
        
    }
};