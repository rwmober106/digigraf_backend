'use strict';
const fs = require('fs');
const FormData = require("form-data");
const path = require('path')
const axios = require("axios")
const pinataSDK = require('@pinata/sdk');


// const IpfsHttpClient = require('ipfs-http-client')
// const ipfs = IpfsHttpClient('http://127.0.0.1:5001')
// const { globSource } = IpfsHttpClient

exports.uploadAssetToIPFS = async function(req, res) {
    //handles null error
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Please provide all required field' });
    }else{
        let customObj = {
            celebrity_id: req.body.celebrity_id,
            autograph_name: req.body.autograph_name,
            autograph_desc: req.body.autograph_desc,            
            image_url: '',
            price: req.body.price
        }
        if (req.body.base64image) {           
            
            var bitmap = Buffer.from(req.body.base64image, 'base64');
            var time = Date.now()
            var baseFileName = time + '.png';
            var rootPath = path.dirname(require.main.filename);
            var filePath = path.join(rootPath, 'images', 'nft', baseFileName)
            console.log('------UPLOADING STARTED--------\n\n');
            fs.writeFileSync(filePath, bitmap, function(err){
            });
            customObj.image_url = baseFileName;
            
            
            let hash = "";
            // const pinata = pinataSDK(process.env.PINATAAPIKEY, process.env.PINATASECRETEAPIKEY)
            const pinata = pinataSDK("c9b60a2023b337081160", "75f6ec4b111c07bbb3c8fc9f73bead8205284a6c2812faf5064c4e73e966ee06");

            const readableStreamForFile = fs.createReadStream(filePath);
            const options = {
                pinataMetadata: {
                    name: customObj.autograph_name,
                    keyvalues: {
                        'description': customObj.autograph_desc,                        
                    }
                },
                pinataOptions: {
                    cidVersion: 0
                }
            };
            pinata.pinFileToIPFS(readableStreamForFile, options).then((result) => {
                //handle results here
                console.log(result);
                console.log('Image Uploading End')
                hash = result.IpfsHash;

                let mdFileName = customObj.autograph_name + '.json';
                let metadata = {"name": customObj.autograph_name, "description": customObj.autograph_desc, "image": `https://gateway.pinata.cloud/ipfs/${hash}`};
                const options1 = {
                    pinataMetadata: {
                        name: mdFileName,
                        keyvalues: {
                            'description': customObj.autograph_desc
                        }
                    },
                    pinataOptions: {
                        cidVersion: 0
                    }
                };
                pinata.pinJSONToIPFS(metadata, options1).then((result) => {
                    //handle results here
                    console.log(result);
                    console.log('Metadata Uploading End');

                    res.status(200).send({error:false, message:"Autograph added successfully", data: result.IpfsHash});

                }).catch((err) => {
                    //handle error here
                    console.log(err);
                });

            }).catch((err) => {
                //handle error here
                console.log(err);
            });                        
            
        }

        // const fileUpload = async function(err, result) {
        //     const fileData = fs.readFileSync(filePath)
        //     const upload = await ipfs.add(fileData);
        //     // const upload = await ipfs.add(globSource(filePath, { recursive: true }));
        //     console.log(upload);
        //     console.log(`https://ipfs.io/ipfs/${upload.cid.toString()}/${url}`);           
                
        //     let metadata = JSON.stringify({
        //         "name": customObj.autograph_name,
        //         "description": customObj.autograph_desc,
        //         "image": `https://ipfs.io/ipfs/${upload.cid.toString()}/${url}`
        //     }, null, '\t');
        //     var metadataFilePath = path.join(rootPath, 'images', 'nft', 'metadata.json')

        //     fs.writeFileSync(metadataFilePath, metadata, function(err){
        //         console.log("++++++", err);
        //     });
        //     console.log(metadataFilePath)
        //     const md = fs.readFileSync(metadataFilePath)
        //     console.log(md)
        //     const metadataUpload = await ipfs.add(fileData);
        //     console.log("++++", metadataUpload);
            
        // }
        // fileUpload()  
        
    }
};