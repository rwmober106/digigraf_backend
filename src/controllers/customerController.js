'use strict';
const Customer = require('../models/customer_model');
const fs = require('fs');
const path = require('path')

exports.findAll = function(req, res) {
    Customer.findAll(function(err, customer) {
        console.log('controller')
        if (err)
            res.send(err);

        console.log('res', customer);
        res.send(customer);
    });
};
exports.create = function(req, res) {    
    //handles null error
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Please provide all required field' });
    }else{
        let customObj = {
            wallet_address: req.body.wallet_address,
            customer_name: '',
            avatar_url: ''
        }
        const new_customer = new Customer(customObj);
        Customer.create(new_customer, function(err, customer) {
        if (err)
            res.send(err);
        res.status(200).send({error: false, message:"Customer added successfully!", data: customer});
    });
    }
};
exports.findById = function(req, res) {
    Customer.findById(req.query.customer_id, function(err, customer) {
        if (err)
            res.send(err);
        res.json({error:false,message:"Success",data:customer});
    });
};
exports.update = function(req, res) {
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Please provide all required field' });
    }else{
        let customObj = {
            wallet_address: req.body.wallet_address,
            customer_name: req.body.customer_name,
            avatar_url: ''
        }

        if (req.body.base64image) {
            var bitmap = Buffer.from(req.body.base64image, 'base64');
            var url = Date.now() + '.png';
            var rootPath = path.dirname(require.main.filename);
            var filePath = path.join(rootPath, 'images', 'avatar', url)
            console.log(filePath);
            fs.writeFileSync(filePath, bitmap, function(err){
                console.log("++++++", err);
            });
            customObj.avatar_url = url;
        }
        console.log(customObj)
        Customer.update(req.params.id, new Customer(customObj), function(err, customer) {
            if (err)
                res.send(err);
            res.json({ error:false, message: 'Customer successfully updated', data: customer });
        });
    }
};
exports.delete = function(req, res) {
    Customer.delete( req.params.id, function(err, customer) {
        if (err)
            res.send(err);
        res.json({ error:false, message: 'Customer successfully deleted' });
    });
};
