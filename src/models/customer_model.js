'use strict';
var dbConn = require('./../../config/db.config');
//Customer object create
var Customer = function(customer){
    this.customer_id          = customer.customer_id
    this.wallet_address       = customer.wallet_address;
    this.customer_name        = customer.customer_name;  
    this.avatar_url           = customer.avatar_url;
};
Customer.create = function (newCus, result) {
    dbConn.query("SELECT * FROM tbl_customer WHERE wallet_address = ?", newCus.wallet_address, function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            console.log(res);
            if (res.length == 0) {
                dbConn.query("INSERT INTO tbl_customer set ?", newCus, function (err, res) {
                    if(err) {
                        console.log("error: ", err);
                        result(err, null);
                    } else {
                        newCus.customer_id = res.insertId
                        console.log(newCus)
                        result(null, newCus);
                    }
                });
            } else {
                result(null, res[0]);
            }
            
           
        }
    })
    
};
Customer.findById = function (id, result) {
    dbConn.query("Select * from tbl_customer where customer_id = ? ", id, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res[0]);
        }
    });
};
Customer.findAll = function (result) {
    dbConn.query("Select * from tbl_customer", function (err, res) {
        if(err) {
           console.log("error: ", err);
           result(err, null);
        }
        else{
           console.log('tbl_customer : ', res);
           result(null, res);
        }
    });
};

Customer.update = function(id, customer, result){
    dbConn.query("UPDATE tbl_customer SET wallet_address=?, customer_name=?, avatar_url=? WHERE customer_id = ?", [customer.wallet_address,customer.customer_name,customer.avatar_url, id], function (err, res) {
        if(err) {
          console.log("error: ", err);
          result(err, null);
        }else{
            customer.customer_id = id;
            result(null, customer);
        }
    });
};
Customer.delete = function(id, result){
    dbConn.query("DELETE FROM tbl_customer WHERE id = ?", [id], function (err, res) {
        if(err) {
           console.log("error: ", err);
           result(null, err);
        }
        else{
           result(null, res);
        }
    });
};

module.exports= Customer;