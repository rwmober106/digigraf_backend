'use strict';
var dbConn = require('../../config/db.config');

var Request = function(request){  
  this.message             = request.message;
  this.image_url           = request.image_url;
  this.celebrity_id        = request.celebrity_id;  
  this.customer_id         = request.customer_id;  
};
Request.create = function (newReq, result) {
    dbConn.query("INSERT INTO tbl_request set ?", newReq, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};
Request.findByCelebrityId = function (id, result) {
    dbConn.query("SELECT a1.*, a2.customer_name, a2.avatar_url, concat(a3.first_name, ' ', a3.last_name) as celebrity_name, a3.cavatar_url FROM tbl_request as a1 LEFT JOIN tbl_customer as a2 ON a1.customer_id = a2.customer_id LEFT JOIN tbl_celebrity as a3 ON a1.celebrity_id = a3.celebrity_id WHERE a1.celebrity_id = ? AND status = 0", id, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

Request.handleRequest = function(status, id, result){
    dbConn.query("UPDATE tbl_request SET status = ? WHERE request_id = ?", [status, id], function (err, res) {
        if(err) {
          console.log("error: ", err);
          result(err, null);
        }else{
            result(null, res.insertId);
        }
    });
};

Request.getRequestById = function(id, result){
    dbConn.query("SELECT a1.*, a2.customer_name, a2.avatar_url, concat(a3.first_name, ' ', a3.last_name) as celebrity_name, a3.cavatar_url FROM tbl_request as a1 LEFT JOIN tbl_customer as a2 ON a1.customer_id = a2.customer_id LEFT JOIN tbl_celebrity as a3 ON a1.celebrity_id = a3.celebrity_id WHERE a1.customer_id = ?", id, function (err, res){
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
        }
    })
}

module.exports = Request;