'use strict';
var dbConn = require('../../config/db.config');
const bcrypt = require('bcrypt');
//Celebrity object create
var Celebrity = function(celebrity){
    this.celebrity_id         = celebrity.celebrity_id;
    this.first_name           = celebrity.first_name;
    this.last_name            = celebrity.last_name;
    this.email                = celebrity.email;
    this.phone_number         = celebrity.phone_number;
    this.celebrity_type       = celebrity.celebrity_type;
    this.cavatar_url          = celebrity.cavatar_url;
    this.cwallet_address      = celebrity.cwallet_address;
    this.password             = celebrity.password;
};
Celebrity.create = function (new_celebrity, result) {
    dbConn.query("INSERT INTO tbl_celebrity set ?", new_celebrity, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else {
            console.log(res.insertId);
            new_celebrity.celebrity_id = res.insertId;
            result(null, new_celebrity);
        }
    });
};
Celebrity.findById = function (id, result) {
    dbConn.query("Select * from tbl_celebrity where celebrity_id = ? ", id, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            sult(null, res);
        }
    });
};
Celebrity.findAll = function (result) {
    dbConn.query("Select * from tbl_celebrity", function (err, res) {
        if(err) {
           console.log("error: ", err);
           result(err, null);
        }
        else{
           console.log('tbl_celebrity : ', res);
           result(null, res);
        }
    });
};
Celebrity.update = function(id, celebrity, result){
    dbConn.query("UPDATE tbl_celebrity SET first_name=?,last_name=?,email=?,phone_number=?,celebrity_type=?,cavatar_url=?, cwallet_address=? WHERE celebrity_id = ?", [celebrity.first_name,celebrity.last_name,celebrity.email,celebrity.phone_number,celebrity.celebrity_type,celebrity.cavatar_url, celebrity.cwallet_address, id], function (err, res) {
        if(err) {
           console.log("error: ", err);
           result(err, null);
        }else{
           result(null, res);
        }
    });
};
Celebrity.delete = function(id, result){
    dbConn.query("DELETE FROM tbl_celebrity WHERE celebrity_id = ?", [id], function (err, res) {
        if(err) {
           console.log("error: ", err);
           result(err, null);
        }
        else{
           result(null, res);
        }
    });
};

Celebrity.login = function(customeObj, result){
    dbConn.query("SELECT * FROM tbl_celebrity WHERE email = ?", [customeObj.email], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else {        
            console.log(res[0].password);     
            bcrypt.compare(customeObj.password, res[0].password)
                .then(result => {
                    console.log("Password compare", result);                    
                });            
            result(null, res[0]);
        }
    });
    
};

module.exports= Celebrity;