const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

//create express app
const app = express();
const path = require('path');
const router = express.Router();

//set up server port
const port = process.env.PORT || 8000;

//parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ limit: "9091990mb", extended: true, parameterLimit: 100000 }));

//parse requests of content-type - application/json
app.use(bodyParser.json({limit: "9091990mb"}));

app.use(cors());

const customerRoutes = require('./src/routes/customer_route');

app.use('/api/v1/customer', customerRoutes)

const celebrityRoutes = require('./src/routes/celebrity_route');

app.use('/api/v1/celebrity', celebrityRoutes);

const requestRoutes = require('./src/routes/request_route');

app.use('/api/v1/request', requestRoutes);

const autographRoutes = require('./src/routes/autograph_route');
// const router = require('./src/routes/customer_route');

app.use('/api/v1/autograph', autographRoutes);

app.set("views", path.join(__dirname, "/src/views"));
app.engine("html", require('ejs').renderFile);
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, '')))
router.get("/", (req, res)=>{
  res.render("index.html");
});
router.get("/signup", (req, res)=>{
  res.render("signup.html");
});
app.use('/', router)

  // listen for requests
const server = app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});